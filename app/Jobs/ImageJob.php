<?php

namespace App\Jobs;

use App\Actions\Image\ApplyFilterAction;
use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function __construct(
        protected ApplyFilterAction $applyFilter,
        protected Request $request,
    )
    {}

    public function handle()
    {
        $image = new Image(
            $this->request->imageId,
            $this->request->src,
        );
        $this->applyFilter->execute($image, $this->request->filter);
    }
}
