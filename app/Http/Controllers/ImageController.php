<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Jobs\ImageJob;
use Illuminate\Http\Request;
use App\Values\Image;
use App\Actions\Image\{
    ApplyFilterAction,
    GetFiltersAction,
};

class ImageController extends Controller
{
    private ApplyFilterAction $applyFilter;
    private GetFiltersAction $getFilters;

    public function __construct(
        ApplyFilterAction $applyFilter,
        GetFiltersAction $getFilters
    ) {
        $this->applyFilter = $applyFilter;
        $this->getFilters = $getFilters;
    }

    public function updateImage(Request $request)
    {

        $result = ImageJob::dispatch($this->applyFilter, $request);
        return response()->json($result->toArray());
    }

    public function filters()
    {
        return response()->json($this->getFilters->execute());
    }
}
